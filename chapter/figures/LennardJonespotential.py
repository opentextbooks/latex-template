import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
from myst_nb import glue

def U(r):
    # Returns U(r), in units where sigma = 1 and epsilon = 1.
    return 4 * (np.power(r, -12) - np.power(r, -6))

# Define r_eq, in units where sigma = 1 and epsilon = 1.
req = np.power(2, 1/6)

fig, ax = plt.subplots(figsize=(6,4))

r = np.linspace(0.01, 3, 299)

line1 = ax.plot(r, U(r), linewidth=3)

ax.axhline(y=0, color='k', linestyle='--')

# Lines to plot from v-axis and U-axis.
ax.plot([req, req], [0, U(req)], color='k', linestyle = ':')
ax.plot([0, req], [U(req), U(req)], color='k', linestyle = ':')

# Point at r_eq.
ax.plot(req, 0, 'o')

# Label the point.
ax.text(req, 0.2, '$r_\\mathrm{eq} = 2^{1/6} \\sigma$', color='C1', horizontalalignment='left')

# Axis settings
ax.set_xlim(0, 2.7)
ax.set_ylim(-1.2,4.2)
ax.set_xlabel('$r/\\sigma$')
ax.set_ylabel('$U/\\varepsilon$')
